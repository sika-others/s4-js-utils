s4 javascript utils
===================

drive utils
-----------

### s4Upload

Uploads single file to s4

    s4Upload(url, file, callback)

example

    file = $("input:file")[0].files[0];
    s4Upload("http://drive.s4.com/", file, function(data){
        console.log(data);
    })

### s4MultiUpload

Uploads list od files to s4

    s4MultiUpload(url, filesList, callback)

example

    file1 = $("#file1")[0].files[0];
    file2 = $("#file2")[0].files[0];
    s4Upload("http://drive.s4.com/", [file1, file2], function(data){
        console.log(data);
    })  

